from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import main
import numpy as np
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

class Ui_App(QtWidgets.QWidget):
    def __init__(self):
        super(Ui_App, self).__init__()
        self.w = self.width()
        self.layout = QtWidgets.QGridLayout()
        self.onlyInt = QtGui.QIntValidator()

        self.title = QtWidgets.QLabel("GAME OF LIFE")
        self.title.setAlignment(QtCore.Qt.AlignHCenter)
        font = QtGui.QFont()
        font.setFamily("Copperplate")
        font.setPointSize(64)
        font.setItalic(False)
        self.title.setFont(font)
        self.layout.addWidget(self.title, 0, 0, 1, 2)

        self.label_2 = QtWidgets.QLabel("SETTING:")
        font2 = QtGui.QFont()
        font2.setPointSize(24)
        self.label_2.setFont(font2)
        self.label_2.setAlignment(QtCore.Qt.AlignHCenter)
        self.layout.addWidget(self.label_2, 1, 0, 1, 2)


        self.label_3 = QtWidgets.QLabel("width: ")
        self.label_3.setAlignment(QtCore.Qt.AlignRight)
        self.layout.addWidget(self.label_3, 2, 0)

        self.textEdit = QtWidgets.QLineEdit()
        self.textEdit.setMaximumHeight(20)
        self.textEdit.setMaximumWidth(70)
        self.textEdit.setAlignment(QtCore.Qt.AlignLeft)
        self.textEdit.setValidator(self.onlyInt)
        self.layout.addWidget(self.textEdit, 2, 1)

        self.label_4 = QtWidgets.QLabel("hight: ")
        self.label_4.setAlignment(QtCore.Qt.AlignRight)
        self.layout.addWidget(self.label_4, 3, 0)

        self.textEdit_2 = QtWidgets.QLineEdit()
        self.textEdit_2.setMaximumHeight(20)
        self.textEdit_2.setMaximumWidth(70)
        self.textEdit_2.setAlignment(QtCore.Qt.AlignLeft)
        self.textEdit_2.setValidator(self.onlyInt)
        self.layout.addWidget(self.textEdit_2, 3, 1)

        self.pushButton = QtWidgets.QPushButton("Random map")
        self.pushButton.setFixedWidth(int(self.w / 2)-100)
        self.layout.addWidget(self.pushButton, 4, 0)

        self.pushButton_2 = QtWidgets.QPushButton("Create own map")
        self.pushButton_2.setFixedWidth(int(self.w/2)-100)
        self.layout.addWidget(self.pushButton_2, 4, 1)

        self.pushButton.setEnabled(False)
        self.pushButton_2.setEnabled(False)

        self.setLayout(self.layout)
        self.show()

        self.pushButton_2.clicked.connect(self.select_own_map)
        self.pushButton.clicked.connect(self.select_random_map)
        self.textEdit.textChanged.connect(self.unlock)
        self.textEdit_2.textChanged.connect(self.unlock)

    def select_random_map(self):
        main.Map(0, int(self.textEdit_2.text()), int(self.textEdit.text()))


    def select_own_map(self):
        main.Map(1, int(self.textEdit_2.text()), int(self.textEdit.text()))

    def unlock(self):
        if len(self.textEdit.text()) > 0 and len(self.textEdit_2.text()) > 0:
            self.pushButton.setEnabled(True)
            self.pushButton_2.setEnabled(True)



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    ex = Ui_App()
    ex.show()
    sys.exit(app.exec_())
