from PyQt5 import QtCore, QtGui, QtWidgets
import numpy as np
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

class Map(QtWidgets.QDialog):
    def __init__(self, map_type, size_x, size_y):
        super(Map, self).__init__()
        self.layout = QtWidgets.QGridLayout()
        self.onlyInt = QtGui.QIntValidator()
        self.mapa = []
        self.map_type = map_type
        self.end = False

        self.label = QtWidgets.QLabel("number of iterations:")
        self.label.setAlignment(QtCore.Qt.AlignHCenter)
        self.layout.addWidget(self.label, 0, 0)

        self.textEdit = QtWidgets.QLineEdit()
        self.textEdit.setMaximumHeight(20)
        self.textEdit.setMaximumWidth(70)
        self.textEdit.setAlignment(QtCore.Qt.AlignLeft)
        self.textEdit.setValidator(self.onlyInt)
        self.layout.addWidget(self.textEdit, 0, 1)

        self.label_2 = QtWidgets.QLabel("speed:")
        self.label_2.setAlignment(QtCore.Qt.AlignHCenter)
        self.layout.addWidget(self.label_2, 0, 2)

        self.slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.slider.setMinimum(1)
        self.slider.setMaximum(20)
        self.slider.setValue(10)
        self.layout.addWidget(self.slider, 0, 3)

        self.pushButton = QtWidgets.QPushButton("START")
        self.layout.addWidget(self.pushButton, 0, 4)
        self.pushButton.clicked.connect(self.start)
        self.pushButton.setEnabled(False)

        self.pushButton_2 = QtWidgets.QPushButton("STOP")
        self.pushButton_2.setDown(False)
        self.layout.addWidget(self.pushButton_2, 0, 5)



        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        self.layout.addWidget(self.canvas, 1, 0, 1, 6)

        if map_type == 0:
            self.random_map(size_x, size_y)
        else:
            self.own_map(size_x, size_y)

        self.textEdit.textChanged.connect(self.unlock)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setLayout(self.layout)

        self.show()
        self.exec_()

    def own_map(self, size_x, size_y):
        self.mapa = np.random.choice(1, size=(size_x, size_y))
        self.draw_map(size_x, size_y)
        self.figure.canvas.mpl_connect('button_press_event', self.onpick)

    def random_map(self, size_x, size_y):
        self.mapa = np.random.choice(2, size=(size_x, size_y), p=[0.9, 0.1])
        self.draw_map(size_x, size_y)

    def start(self):
        self.textEdit.setEnabled(False)
        self.slider.setEnabled(False)
        self.pushButton.setEnabled(False)
        for i in range(int(self.textEdit.text())):
            self.mapa = get_neighbors(self.mapa)
            self.draw_map(self.mapa.shape[0], self.mapa.shape[1])
            if self.pushButton_2.isDown() == True:
                break
            self.canvas.flush_events()
            plt.pause(self.slider.value()/10)
        self.textEdit.setEnabled(True)
        self.slider.setEnabled(True)
        self.pushButton.setEnabled(True)
        self.pushButton_2.setDown(False)

    def draw_map(self, size_x, size_y):
        ax = self.figure.add_subplot(111)
        ax.clear()
        ax.imshow(self.mapa, cmap='Greys')
        tab1 = []
        for i in range(0, size_x):
            tab1.append(0.5 + i)
        tab2 = []
        for i in range(0, size_y):
            tab2.append(0.5 + i)
        ax.set_xticks(tab2)
        ax.set_yticks(tab1)
        ax.grid(b=True, which='major', color='#666666', linestyle='-')
        ax.grid(b=True, which='minor', color='#666666', linestyle='-')
        ax.tick_params(colors='white')
        self.canvas.draw()

    def onpick(self, event):
        if self.mapa[int(round(event.ydata))][int(round(event.xdata))] == 0:
            self.mapa[int(round(event.ydata))][int(round(event.xdata))] = 1
        else:
            self.mapa[int(round(event.ydata))][int(round(event.xdata))] = 0
        self.draw_map(self.mapa.shape[0], self.mapa.shape[1])

    def unlock(self):
        self.pushButton.setEnabled(True)

def get_neighbors(mapa):
    new_map = np.zeros((mapa.shape[0]+2, mapa.shape[1]+2))
    new_map[1:-1, 1:-1] = mapa

    for i in range(1, new_map.shape[0]-1):
        for j in range(1, new_map.shape[1]-1):
            counter = 0
            miniMap = new_map[i - 1:i + 2, j - 1:j + 2]
            for k in range(9):
                if miniMap.item(k) == 1:
                    counter += 1
            counter -= new_map[i][j]
            if counter == 3:
                mapa[i-1][j-1] = 1
            elif counter < 2 or counter > 3:
                mapa[i-1][j-1] = 0

    return mapa








