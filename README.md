# GAME OF LIFE
Gra w życie wykonana za pomocą Pythona.<br/>
Użyte biblioteki:
*  PyQt
*  Numpy
*  Matplotlib

|![picture](Pictures/Screenshot_1.png) |![picture](Pictures/Screenshot_2.png)  |
| ------ | ------ |

![picture](Pictures/gif1.gif)
![picture](Pictures/gif2.gif)
